import os
import sys
import time
import signal
from datetime import datetime
import RPi.GPIO as GPIO
from evdev import uinput, UInput, ecodes as e

# Settings
BOUNCE_TIME = 0.01 # seconds

# Test
# UP_BTN    = 26
# DOWN_BTN  = 19
# LEFT_BTN  = 13
# RIGHT_BTN = 6
# A_BTN     = 0
# B_BTN     = 11
# X_BTN     = 9
# Y_BTN     = 10
# START     = 22
# SELECT    = 27
# RTRIG     = 17
# LTRIG     = 4

# GPIO defintions for buttons
UP_BTN    = 30
DOWN_BTN  = 31
LEFT_BTN  = 32
RIGHT_BTN = 33
A_BTN     = 34
B_BTN     = 35
X_BTN     = 36
Y_BTN     = 37
START     = 38
SELECT    = 39
RTRIG     = 42
LTRIG     = 43

BUTTONS = [A_BTN, B_BTN, X_BTN, Y_BTN, SELECT, START, RTRIG, LTRIG, UP_BTN, DOWN_BTN, LEFT_BTN, RIGHT_BTN]

KEYS = {# EDIT KEYCODES IN THE TABLE TO YOUR PREFERENCES:
        # See /usr/include/linux/input.h for keycode names
        #Button     # Keyboard          EmulationStation
        RTRIG:      e.KEY_W,            #'RT' button
        LTRIG:      e.KEY_Q,            #'LT' button
        A_BTN:      e.KEY_X,            # 'A' button
        B_BTN:      e.KEY_Z,            # 'B' button
        X_BTN:      e.KEY_S,            # 'X' button
        Y_BTN:      e.KEY_A,            # 'Y' button
        UP_BTN:     e.KEY_UP,           # Dpad Up
        DOWN_BTN:   e.KEY_DOWN,         # Dpad Down
        RIGHT_BTN:  e.KEY_RIGHT,        # Dpad Right
        LEFT_BTN:   e.KEY_LEFT,         # Dpad Left
        SELECT:     e.KEY_LEFTSHIFT,    # Select button
        START:      e.KEY_ENTER         # Start button
}


# GPIO Init
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(BUTTONS, GPIO.IN, pull_up_down=GPIO.PUD_UP)

try:
    ui = UInput({e.EV_KEY: KEYS.values()}, name="game-io controls", bustype=e.BUS_USB)
except uinput.UInputError as e:
    sys.stdout.write(e.message)
    sys.stdout.write("are you root?") 
    sys.exit(0)

def handle_button(channel):
    key = KEYS[channel]
    time.sleep(BOUNCE_TIME)
    state = 0 if GPIO.input(channel) else 1
    ui.write(e.EV_KEY, key, state)
    ui.syn()

for button in BUTTONS:
    GPIO.add_event_detect(button, GPIO.BOTH, callback=handle_button, bouncetime=1)


while True:
    time.sleep(3)